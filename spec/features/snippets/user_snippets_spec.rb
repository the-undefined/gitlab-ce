require 'rails_helper'

feature 'User Snippets', feature: true do
  background do
    author = create(:user)
    public_snippet = create(:personal_snippet, :public, author: user)
    internal_snippet = create(:personal_snippet, :internal, author: user)
    private_snippet = create(:personal_snippet, :private, author: user)

    login_as author
    visit dashboard_snippets_path(public_snippet)
  end

  scenario 'View all of my snippets' do
    expect(page).to have_content(public_snippet.content)
    expect(page).to have_content(internal_snippet.content)
    expect(page).to have_content(private_snippet.content)
  end

  scenario 'View my public snippets' do
    page.within('.snippet-scope-menu') do
      click_link "Public"
    end

    expect(page).to have_content(public_snippet.content)
    expect(page).not_to have_content(internal_snippet.content)
    expect(page).not_to have_content(private_snippet.content)
  end

  scenario 'View my internal snippets' do
    page.within('.snippet-scope-menu') do
      click_link "Internal"
    end

    expect(page).not_to have_content(public_snippet.content)
    expect(page).to have_content(internal_snippet.content)
    expect(page).not_to have_content(private_snippet.content)
  end

  scenario 'View my private snippets' do
    page.within('.snippet-scope-menu') do
      click_link "Private"
    end

    expect(page).not_to have_content(public_snippet.content)
    expect(page).not_to have_content(internal_snippet.content)
    expect(page).to have_content(private_snippet.content)
  end
end
